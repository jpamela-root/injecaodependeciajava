package injecaodependecia.di;

import injecaodependecia.clipro.Cliente;
import injecaodependecia.notificacao.Notificador;
import injecaodependecia.notificacao.NotificadorEmail;
import injecaodependecia.service.AtivacaoClienteService;

public class Main {

	public static void main(String[] args) {
	Cliente fulano = new Cliente ("fulano", "fulano@abc.com", "741852963");
	Cliente beltrano = new Cliente ("beltrano", "beltrano@abc.com", "369852147");
  
	Notificador notificador = new NotificadorEmail();
	// Notificador notificador = new NotificadorSMS();
	
	AtivacaoClienteService ativacaoCliente = new AtivacaoClienteService(notificador);
	ativacaoCliente.ativar(fulano);
	ativacaoCliente.ativar(beltrano);
	}
}