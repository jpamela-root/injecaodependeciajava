package injecaodependecia.notificacao;

import injecaodependecia.clipro.Cliente;

public interface Notificador {

	void notificar(Cliente cliente, String mensagem);
}

