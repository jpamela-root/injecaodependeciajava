package injecaodependecia.service;

import injecaodependecia.clipro.Cliente;
import injecaodependecia.notificacao.Notificador;
// import injecaodependecia.notificacao.NotificadorEmail;
import injecaodependecia.notificacao.NotificadorSMS;

public class AtivacaoClienteService {

	private Notificador notificador;
	
	public AtivacaoClienteService(Notificador notificador) {
		this.notificador = notificador;
		
	}
	
	public void ativar (Cliente cliente) {
		cliente.ativar();

		// NotificadorSMS notificador = new NotificadorSMS();
		notificador.notificar(cliente, "Seu cadastro no sistema está ativo!");
		

		
	}	
}
