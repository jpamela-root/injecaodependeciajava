package injecaodependecia.service;

import injecaodependecia.clipro.Cliente;
import injecaodependecia.clipro.Produto;
import injecaodependecia.notificacao.Notificador;
// import injecaodependecia.notificacao.NotificadorEmail;
import injecaodependecia.notificacao.NotificadorSMS;

public class EmitirnotafiscalService {

	private Notificador notificador;
	
	public EmitirnotafiscalService(Notificador notificador) {
		this.notificador = notificador;
	}
	
	public void emitir(Cliente cliente, Produto produto) {
	//TODO emite nota fiscal aqui....
		
	//NotificadorSMS notificador = new NotificadorSMS();
		
		
		this.notificador.notificar(cliente, "Nota fiscal do produto "
		     + produto.getNome() + "foi emitida!");
		    
	}
	
	
	
}
